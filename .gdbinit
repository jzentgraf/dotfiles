set follow-fork-mode child
set print asm-demangle
set print demangle

set history save on
set history filename ~/.gdb_history
set history size 2000
