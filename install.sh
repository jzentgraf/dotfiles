#!/bin/bash
BASEDIR="$(dirname "$(readlink -f "$0")")"
DOTFILES=(
    ".bash_profile"
    ".bashrc"
    ".config/dircolors"
    ".config/nvim/init.vim"
    ".config/pacman/makepkg.conf"
    ".config/pikaur.conf"
    ".config/tmux/tmux.conf"
    ".config/user-dirs.dirs"
    ".config/user-dirs.locale"
    ".config/zsh/.zprofile"
    ".config/zsh/.zshrc"
    ".local/bin/bwlimit"
    ".local/bin/crc32"
    ".local/bin/localdns"
    ".local/bin/partcarve"
    ".local/bin/wineenv"
    ".local/share/mime/packages/x-btsnoop.xml"
    ".local/share/mime/packages/x-saleae-sal.xml"
    ".gdbinit"
    ".local/share/konsole/Breeze.colorscheme"
    ".local/share/konsole/Default.profile"
    ".profile"
    ".vimrc"
    ".zshenv"
)
DOTDIRS=(
    ".vim/pack/ahf/start/cocci-syntax"
    ".vim/pack/vim-airline/start/vim-airline"
    ".vim/pack/dense-analysis/start/ale.nvim"
    ".vim/pack/jonathanfilip/opt/vim-lucius"
    ".vim/pack/junegunn/start/vim-easy-align"
    ".vim/pack/junegunn/opt/vim-emoji"
    ".vim/pack/neoclide/opt/coc.nvim"
    ".vim/pack/rust-lang/start/rust.vim"
    ".vim/pack/tpope/start/vim-abolish"
    ".vim/pack/tpope/start/vim-commentary"
    ".vim/pack/tpope/start/vim-fugitive"
    ".vim/pack/tpope/start/vim-sleuth"
    ".vim/pack/ycm-core/opt/YouCompleteMe"
    ".vim/pack/vim-syntastic/start/syntastic"
    ".vim/pack/yatli/sleigh.vim"
)

# Ensure all submodules have been checked out
echo "[+] Updating submodules"
git submodule update --init --recursive

# Install links to configuration files, backing up if necessary.
cd "$BASEDIR"
for item in "${DOTFILES[@]}" "${DOTDIRS[@]}"; do
    # Skip if the link is already installed
    if [ "$(readlink -f "$HOME/$item")" != "$(readlink -f "$item")" ]; then
        echo "[+] Installing $item"

        # Call ln with -t to avoid descending into existing directory links
        mkdir -p "$(dirname "$HOME/$item")"
        ln -sr --backup=numbered -t "$(dirname "$HOME/$item")" "$item"
    fi
done

# Move existing .gitconfig to ~/config/git/config
mkdir -p "$HOME/.config/git"
if [ ! -f "$HOME/.config/git/config" ] && [ -f "$HOME/.gitconfig" ]; then
    mv "$HOME/.gitconfig" "$HOME/.config/git/config"
fi

# Allow Git configuration to be easily overridden
if ! grep -F "$BASEDIR/.config/git/config" "$HOME/.config/git/config" >/dev/null 2>&1; then
(
    echo "[+] Generating Git config"

    # Prepend .config/git/config with an include directive
    tmpfile="$(mktemp)"
    cat - "$HOME/.config/git/config" >"$tmpfile" 2>/dev/null <<EOF
[include]
	path = $BASEDIR/.config/git/config
EOF

    mv --backup=numbered "$tmpfile" "$HOME/.config/git/config"
)
fi

#echo "[+] Building YouCompleteMe"
#(
#    cd "$BASEDIR/.vim/pack/ycm-core/start/YouCompleteMe" || exit 1
#    python3 install.py --clangd-completer
#)

echo "[+] Generating vim helptags"
(
    # Set nullglob in a subshell in case no doc directories match a pattern
    shopt -s nullglob
    for docdir in ~/.vim/pack/*/{start,opt}/*/doc; do
        vim --cmd "helptags $docdir" --cmd qall
    done
)
