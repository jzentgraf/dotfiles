set nocompatible
set number
set ruler
set cursorline
set mouse=a

set backspace=indent,eol,start
set whichwrap+=<,>,[,],h,l
set scrolloff=3

set secure
set nomodeline

set wildmenu
set wildmode=longest:full,full

set autoread
set hidden

set listchars=eol:$,tab:-->,space:.,extends:\\,nbsp:+

" highlight regex matches during command entry
set incsearch hlsearch
if has('nvim')
    set inccommand=split
endif

" remove search highlight on <C-L>
nnoremap <silent> <C-L> :nohlsearch<CR><C-L>

" formatting options
syntax enable
filetype plugin indent on

set sts=4 sw=4 et ai sta
au filetype c setl cc=81
au filetype cpp setl cc=121
au filetype python setl cc=81
au filetype markdown setl cc=75 tw=74 sts=2 sw=2
au filetype gitcommit setl cc=73 tw=72
au filetype json setl sts=2 sw=2
au filetype yaml setl sts=2 sw=2
au filetype xml setl sts=2 sw=2

" enable syntax highlighting for files named *.strace
au BufRead,BufNewFile *.strace set filetype=strace

" highlight excess whitespace
au WinEnter,VimEnter * highlight TrailingWhitespace cterm=inverse
au WinEnter,VimEnter * match TrailingWhitespace /\s\+$/

" theming
packadd vim-lucius
let g:lucius_style = 'dark'
let g:lucius_no_term_bg = 1
colorscheme lucius
" Make background transparent
for group in ['Normal', 'LineNr', 'VertSplit', 'SignColumn', 'NonText']
    execute 'hi ' . group . ' ctermbg=NONE'
endfor

" programming helpers
let g:ycm_add_preview_to_completeopt = 1
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_always_populate_location_list = 1


xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

" CoC config
if has("patch-8.1.1564")
    set signcolumn=number
else
    set signcolumn=yes
endif

" Walk through completions with tab
inoremap <expr> <TAB>   pumvisible() ? "\<C-n>" : "\<TAB>"
inoremap <expr> <S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

nnoremap <silent> <Leader>g :YcmCompleter GoToDefinition<CR>
nnoremap <silent> <Leader>d :YcmCompleter GoToDeclaration<CR>
nnoremap <silent> <Leader>y :YcmCompleter GoToType<CR>
nnoremap <silent> <Leader>i :YcmCompleter GoToImplementation<CR>
nnoremap <silent> <Leader>r :YcmCompleter GoToReferences<CR>
nnoremap <silent> <Leader>o :YcmCompleter GoToDocumentOutline<CR>
nmap <Leader>p <Plug>(YCMFindSymbolInWorkspace)

nnoremap <Leader>n :YcmCompleter RefactorRename 
nnoremap <Leader>f :YcmCompleter FixIt<CR>

nnoremap <silent> <Leader>? :call <SID>show_documentation()<CR>
function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (has('nvim') && &filetype == "man")
    call <SID>show_manpage()
  else
    YcmCompleter GetDoc
  "elseif (coc#rpc#ready())
  "  call CocActionAsync('doHover')
  "else
  "  execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

function! s:show_manpage()
  let manpage = matchstr(expand('<cWORD>'), '[A-Za-z0-9_.@:+-]\+(\d)')
  if !empty(manpage)
    execute 'e man://'.expand(manpage)
  endif
endfunction
