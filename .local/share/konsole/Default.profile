[Appearance]
ColorScheme=Breeze

[General]
Name=Default
Parent=FALLBACK/
TerminalMargin=4

[Scrolling]
HighlightScrolledLines=false
HistorySize=8000
ScrollBarPosition=2

[Terminal Features]
ReverseUrlHints=true
UrlHintsModifiers=67108864
VerticalLine=false
