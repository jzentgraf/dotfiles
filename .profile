export ANDROID_HOME="$HOME/.android"
export PATH="$PATH:$HOME/.local/bin"
export EDITOR=nvim
export MANPAGER='nvim +Man!'
export BROWSER=firefox
export SEMGREP_SEND_METRICS=off

if [ "$(lsb_release -is 2>/dev/null)" == "Debian" ]; then
    # Debian doesn't put sbin in user PATH by default...
    export PATH="$PATH:/sbin:/usr/sbin"
fi

(command -v gpg-connect-agent && gpg-connect-agent updatestartuptty /bye) >/dev/null 2>&1
export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"

umask 022
