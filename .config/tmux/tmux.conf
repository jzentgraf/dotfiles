setw -g mode-keys vi

set -g mouse on
set -g escape-time 0
set -g history-limit 20000
set -g display-time 5000
set -g focus-events on

set -g status on
set -g status-interval 2
set -g status-justify left
set -g status-position bottom
set -g status-left-length 60
set -g status-right-length 60
set -g status-right " #(echo \${USER:-\$(id -un)})@#(echo \${HOSTNAME:-\$(hostname)})  %Y-%m-%d %H:%M"

set -g status-style fg=#303030,bg=#b2b2b2

setw -g window-status-format "[#I #W#F]"
setw -g window-status-current-format "#[reverse][#I #W#F]"

set -g update-environment 'DISPLAY SSH_ASKPASS SSH_AGENT_PID SSH_CONNECTION WAYLAND_DISPLAY WINDOWID XAUTHORITY TERM'

# Fix color support
set -g default-terminal tmux
if '[ "${TERM%-256color}" != "${TERM}" ] || [ "${TERM}" == fbterm ]' 'set -g default-terminal tmux-256color'

bind -r H resize-p -L 1
bind -r J resize-p -D 1
bind -r K resize-p -U 1
bind -r L resize-p -R 1

# add sane key binds for splits
bind | split-window -h -c "#{pane_current_path}"
bind - split-window -v -c "#{pane_current_path}"
bind % split-window -h -c "#{pane_current_path}"
bind '"' split-window -v -c "#{pane_current_path}"

# make copy mode behave more similarly to vim
bind -n -T copy-mode-vi v   send-keys -X begin-selection
bind -n -T copy-mode-vi V   send-keys -X select-line
bind -n -T copy-mode-vi C-v send-keys -X rectangle-toggle
bind -n -T copy-mode-vi y   send-keys -X copy-selection-and-cancel
# if we can, sync copy buffers to the clipboard
# support wl-clip for Wayland, xsel/xclip for X11, pbcopy for OS X, and clip.exe for WSL
if "[ -n \"${DISPLAY}\" ] && command -v xclip" {
    bind -n -T copy-mode-vi y   send-keys -X copy-pipe-and-cancel 'xclip -i -sel clip'
}
if "[ -n \"${DISPLAY}\" ] && command -v xsel" {
    bind -n -T copy-mode-vi y   send-keys -X copy-pipe-and-cancel 'xsel -i -b'
}
if "[ -n \"${WAYLAND_DISPLAY}\" ] && command -v wl-copy" {
    bind -n -T copy-mode-vi y   send-keys -X copy-pipe-and-cancel 'wl-copy'
}
if "command -v pbcopy" {
    bind -n -T copy-mode-vi y   send-keys -X copy-pipe-and-cancel 'pbcopy'
}
if "command -v clip.exe" {
    bind -n -T copy-mode-vi y   send-keys -X copy-pipe-and-cancel 'clip.exe'
}

# open the buffer contents in the default text editor with <prefix>e
bind e run-shell 'tmpfile="$(mktemp)"; tmux capture-pane -p -S- > "$tmpfile"; tmux new-window "${EDITOR:-vim} \"$tmpfile\"; rm \"$tmpfile\""'

# reload tmux configuration
bind r {
    source-file ~/.config/tmux/tmux.conf
    display "Reloaded configuration"
}
