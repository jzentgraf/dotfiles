# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Set shell options
setopt pushd_silent
setopt pushd_to_home

# Set up shell history
HISTFILE="${HOME}/.zsh_history"
HISTSIZE=4000
SAVEHIST=16000

setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_find_no_dups
setopt extended_history
setopt inc_append_history_time
setopt hist_fcntl_lock

# Build the prompt
# Enable Git support - print the current branch or commit hash
# Use color to indicate whether there are staged and/or unstaged changes
autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:git:*' check-for-changes true
zstyle ':vcs_info:git:*' check-for-staged-changes true
zstyle ':vcs_info:git:*' stagedstr '%F{yellow}+'
zstyle ':vcs_info:git:*' unstagedstr '%F{red}~'
zstyle ':vcs_info:git:*' formats '[%F{green}%c%u%b%f]'
zstyle ':vcs_info:git:*' actionformats '[%F{green}%c%u%b%f (%a)]'

#zstyle ':vcs_info:git+post-backend:*' hooks git-branch-name
zstyle ':vcs_info:git+set-message:*' hooks git-untracked
+vi-git-branch-name() {
    local branch tag commit
    if ! hook_com[branch]="$(git symbolic-ref --short -q HEAD 2>/dev/null)"
    branch="$(git symbolic-ref --short -q HEAD 2>/dev/null)"
    commit="$(git rev-parse --short HEAD 2>/dev/null)"
    tag="$(git describe --tags --exact-match HEAD 2>/dev/null)"

    if [ -n "$branch" ]; then
        hook_com[branch]="$branch"
    elif [ -n "$commit" ]; then
        hook_com[branch]="g$commit"
    fi

    if [ -n "$tag" ]; then
        hook_com[branch]="${hook_com[branch]+${hook_com[branch]} }<${tag}>"
    fi
}
+vi-git-untracked() {
    if (git status --porcelain | grep -Fm1 '??') &>/dev/null; then
        hook_com[staged]='%F{yellow}?'
    fi
}

precmd_functions+=( vcs_info )

setopt prompt_subst
unset PROMPT
PROMPTITEMS=(
    '%(?..[%F{red}%B%?%b%f] )'
    '${vcs_info_msg_0_:+${vcs_info_msg_0_} }'
    '[%(!.%F{red}.%F{blue})%n%f${SSH_CONNECTION+%F{yellow\}@%m%f} %F{green}%~%f]'
    '%(!.#.$) '
)
PROMPT="${(j::)PROMPTITEMS}"

# Add a clock to the prompt
#PROMPT="[%F{240}%D{%H:%M}%f] $PROMPT"
#TMOUT=1
#TRAPALRM() { zle reset-prompt }

# Detect new completions (e.g. after program install)
zstyle ':completion:*' rehash true

# Alias common commands to add common flags
alias ls='ls --color=auto -Nv --group-directories-first'
alias dd='dd status=progress'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias inssh='ssh -oKexAlgorithms=+diffie-hellman-group1-sha1 -oHostKeyAlgorithms=+ssh-rsa -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no'
alias inscp='scp -oKexAlgorithms=+diffie-hellman-group1-sha1 -oHostKeyAlgorithms=+ssh-rsa -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no'
# "byte based" dd
# dd "but better"
alias bb='dd iflag=skip_bytes,count_bytes oflag=seek_bytes status=progress'

# The following lines were added by compinstall

zstyle ':completion:*' add-space true
zstyle ':completion:*' completer _expand _complete _ignored _prefix
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' file-sort name
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SShowing %l entries. Press TAB for more.%s
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' matcher-list '' 'r:|[._-]=* r:|=*'
zstyle ':completion:*' menu select
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' select-prompt %SShowing %l entries. Scroll for more.%p
zstyle ':completion:*' squeeze-slashes true
zstyle :compinstall filename "${ZDOTDIR:-$HOME}/.zshrc"

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Fall back to bash completions
autoload -Uz bashcompinit
bashcompinit

# Load dircolors
source <(dircolors "${XDG_CONFIG_HOME:-${HOME}/.config}/dircolors")
zstyle ':completion:*' list-colors "${(@s.:.)LS_COLORS}"

# Set up key binds
bindkey -e

bindkey $'\e[1;5D' backward-word
bindkey $'\e[1;5C' forward-word
[[ -n "${terminfo[kLFT1]}" ]] && bindkey -- "${terminfo[kLFT1]}" backward-word
[[ -n "${terminfo[kRIT1]}" ]] && bindkey -- "${terminfo[kRIT1]}" forward-word

[[ -n "${terminfo[khome]}" ]] && bindkey -- "${terminfo[khome]}" beginning-of-line
[[ -n "${terminfo[kend]}"  ]] && bindkey -- "${terminfo[kend]}"  end-of-line
[[ -n "${terminfo[kcbt]}"  ]] && bindkey -- "${terminfo[kcbt]}"  reverse-menu-complete
[[ -n "${terminfo[kbs]}"   ]] && bindkey -- "${terminfo[kbs]}"   backward-delete-char
[[ -n "${terminfo[kdch1]}" ]] && bindkey -- "${terminfo[kdch1]}" delete-char
[[ -n "${terminfo[kich1]}" ]] && bindkey -- "${terminfo[kich1]}" overwrite-mode
[[ -n "${terminfo[kcuu1]}" ]] && bindkey -- "${terminfo[kcuu1]}" up-line-or-history
[[ -n "${terminfo[kcud1]}" ]] && bindkey -- "${terminfo[kcud1]}" down-line-or-history
[[ -n "${terminfo[kcub1]}" ]] && bindkey -- "${terminfo[kcub1]}" backward-char
[[ -n "${terminfo[kcuf1]}" ]] && bindkey -- "${terminfo[kcuf1]}" forward-char

if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
    function zle-line-init()   { echoti smkx }
    function zle-line-finish() { echoti rmkx }
    zle -N zle-line-init
    zle -N zle-line-finish
fi

# Search history for partial commands when pressing up or down
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

[[ -n "${terminfo[kcuu1]}" ]] && bindkey -- "${terminfo[kcuu1]}"   up-line-or-beginning-search
[[ -n "${terminfo[kcud1]}" ]] && bindkey -- "${terminfo[kcud1]}" down-line-or-beginning-search

# Allow editing the command line in $EDITOR with Ctrl+x, Ctrl+e
autoload -U edit-command-line
zle -N edit-command-line
bindkey '\C-x\C-e' edit-command-line

# Enable autosuggestions if present
if [ -f /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh ]; then
    export ZSH_AUTOSUGGEST_USE_ASYNC=1
    if [ "$(tput colors 2>/dev/null)" = 8 ]; then
        # Use yellow where dark gray is unavailable (e.g. Linux TTY)
        export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=3"
    fi
    source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
fi

# Apply local customizations
if [ -f "${ZDOTDIR:-$HOME}/.zshlocal" ]; then
    source "${ZDOTDIR:-$HOME}/.zshlocal"
fi
